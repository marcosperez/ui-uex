angular.module('uexApp',[
	'ngRoute',
	'uexApp.home'
]).config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/home/home.html"
    })
});