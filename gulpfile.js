var gulp = require('gulp'),
  gutil = require('gulp-util');
// Ejecutar comandos de linea
var exec = require('child_process').exec;

// Modulos
// Agrega dependencias de bower en index.html
var wiredep = require('wiredep').stream;
// Linter 'para detectar errores de javascriopt
var jshint = require('gulp-jshint');
// Permite sincronizar los cambios con el navegador para verlos en tiempo real
var browserSync = require('browser-sync').create();
// Agrega los script en index.html
var inject = require('gulp-inject');
//Agrega las dependencias a angular modules
var moduleDependencies = require('gulp-angular-module-dependencies');

var git = require('gulp-git');

gulp.task('bower', function (event) {
  gutil.log('Cargando nuevas dependencias');
  gutil.log('bower install');
  exec('cd app && bower install && cd..', function (err) {

    if (err) {
      gutil.log("Finished " + err);
      return "error";
    }
    gutil.log('dependencias descargadas');
    gulp.src('app/index.html')
      .pipe(
        wiredep({
          src: 'app/index.html',
          cwd: 'app'
        }))
      .pipe(gulp.dest('app'));
    gutil.log('Finished: se agregaron a index.html');
  });
});

//watch
gulp.task('watch', function () {
  gulp.watch(['app/views/**/*.js'], [
    'inject',
    'add_dependencies_angular'
  ]);
  gulp.watch(['app/views/**/*.js', 'app/*.js'], {
    cwd: './'
  }, ['jshint']);
  gulp.watch(['app/*.css', 'app/static/css/*.css', 'app/views/**/*.css'], {
    cwd: './'
  }, ['inject']);
  gulp.watch(['app/bower.json', 'app/bower_components/**'], {
    cwd: './'
  }, ['bower']);
})
// configure the jshint task
gulp.task('jshint', function () {
  return gulp.src('app/views/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "app"
    }
  });
  gulp.watch(['app/views/**/*.js',
      'app/views/**/*.html',
      'app/views/**/*.js',
      'app/views/**/*.css',
      'app/*.html', 'app/*.js',
      'app/*.css',
      'app/*.html',
      'app/static/css/*.css'
    ])
    .on('change', browserSync.reload);
});

gulp.task('inject', function () {
  gulp.src('app/index.html')
    .pipe(inject(gulp.src([
      'app/views/**/*.js',
      'app/*.js',
      'app/*.css',
      'app/static/css/*.css'
    ]), {
      ignorePath: 'app',
      addRootSlash: false
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('add_dependencies_angular', function () {
  gulp.src(['./app/app.js', "./app/views/**/*.js"])
    .pipe(moduleDependencies('uexApp'))
    .pipe(gulp.dest('app/'));
})


gulp.task('commit', function () {
  gulp.src(['!node_modules/', './*'])
    .pipe(git.add({
      args: '-A'
    }))
    .pipe(git.commit(undefined, {
      args: '-m "commit"',
      disableMessageRequirement: true
    }));
})



gulp.task('publicar', ['commit'], function (done) {
  git.push('origin', 'master', function (err) {
    if (err) throw err;
    gutil.log(gutil.colors.green('************** Git push is done! **************'));
    if (done) done();
  });
});

gulp.task('git', function () {
  setInterval(function () {
    gutil.log(gutil.colors.blue('************** Publicando en git **************'));
    exec('gulp publicar', function (err) {
      if (err) {
        //gutil.log("Finished " + err);
        return "error";
      }
    });
  }, 600000);
});

//Tarea de inicio
gulp.task('default', ['bower','watch', 'browser-sync', 'git']);